LCRP
----

Calculates the best Lipschitz constant *k(p)* for the radial
projection on *L<sup>p</sup>*[0,1] and the space of sequences
with the *p*-norm using the main result of [1].

1. Carlo Franchetti, "The Norm of the Minimum Projection onto
Hyperplanes in *L<sup>p</sup>*[0,1] and the Radial Constant",
Boll. Un. Mat. Ital. 4-B (7), pp. 803-821, 1990.

2. J.J. Green, "The Lipschitz constant for the radial projection
on real *l*<sub>p</sub> -- implementation notes", unpublished
technical note, 2012.

Files
-----

The algorithm is implemented in C in the files `lcrp.h`and
`lcrp.c`, and in Matlab/Octave in `lcrp.m`.
