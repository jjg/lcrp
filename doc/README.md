lcrp documentation
--------------------

The docbook source in `lcrp.xml` is used to generate the Unix
man-page `lcrp.3` and the plain text `lcrp.txt`. The tools
`xsltproc` and `lynx` are required to regenerate these files.

The script `lcrp-fetch.sh` pulls the latest version of `lcrp.c`
and `lcrp.h` from the GitHub master to the current directory.
