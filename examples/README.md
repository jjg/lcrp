Example program
---------------

The file `lcrp-table.c` is an example program
which prints a table of the Lipschitz constant,
compile it with

    make lcrp-table

(note that the compile requires the
[gengetopt](http://www.gnu.org/software/gengetopt/) program).

The script `lcrp-error.py` reads lines of the form

    p k(p)

and prints the error of *k(p)* as compared to a
mutiprecision estimate of it, the *p* should be
in hexedecimal format.  See `err-*.sh` for example
usage -- requires Python and the
[mpmath library](http://code.google.com/p/mpmath/),
