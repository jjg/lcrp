/*
  simple test application for lcrp, prints out
  a table of values of the function

  J.J. Green 2012
*/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#include "lcrp.h"
#include "options.h"

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;
  struct { int p, k; } width, precision;
  bool logarithmic = false, minusone = false, hexp = false;

  if (options(argc,argv,&info) != 0)
    {
      fprintf(stderr,"failed to parse command line\n");
      return EXIT_FAILURE;
    }
 
  width.p     = info.pwidth_arg;
  width.k     = info.kwidth_arg;
  precision.p = info.pprecision_arg;
  precision.k = info.kprecision_arg;
  logarithmic = info.logarithmic_flag;
  minusone    = info.minusone_flag;
  hexp        = info.phex_flag;

  if (info.inputs_num != 3)
    {
      fprintf(stderr,"usage: lcrp-table <min> <max> <n>\n");
      return EXIT_FAILURE;
    }

  double 
    pmin = atof(info.inputs[0]), 
    pmax = atof(info.inputs[1]);
  int np = atoi(info.inputs[2]);

  if (minusone)
    {
      pmin += 1;
      pmax += 1;
    }

  if (logarithmic && (pmin <= 1))
    {
      fprintf(stderr,"pmin > 1 required\n");
      return EXIT_FAILURE;
    }
    
  if (pmin < 1)
    {
      fprintf(stderr,"pmin >= 1 required\n");
      return EXIT_FAILURE;
    }

  if (pmin >= pmax)
    {
      fprintf(stderr,"pmin < pmax required\n");
      return EXIT_FAILURE;
    }

  if (np < 2)
    {
      fprintf(stderr,"np > 1 required\n");
      return EXIT_FAILURE;
    }

  int i;

  char fmt[64];

  sprintf(fmt,"%%%i.%i%c %%%i.%if\n",
	  width.p, precision.p, (hexp ? 'a' : 'f'),
	  width.k, precision.k);

  if (logarithmic)
    {
      double l10qmin = log10(pmin-1), l10qmax = log10(pmax-1);

      for (i=0 ; i<np ; i++)
	{
	  int err;
	  double k, 
	    l10q = (l10qmin*(np-1-i) + l10qmax*i)/(np-1),
	    p = pow(10,l10q) + 1;

	  
	  if ((err = lcrp(p, &k)) != 0)
	    {
	      fprintf(stderr,"error : %s\n",lcrp_strerror(err));
	      return EXIT_FAILURE;
	    }
	  
	  printf(fmt,p,k);
	}
    }
  else
    {
      for (i=0 ; i<np ; i++)
	{
	  int err;
	  double k, p = (pmin*(np-1-i) + pmax*i)/(np-1);
	  
	  if ((err = lcrp(p, &k)) != 0)
	    {
	      fprintf(stderr,"error %s\n",lcrp_strerror(err));
	      return EXIT_FAILURE;
	    }
	  
	  printf(fmt,p,k);
	}
    }

  return EXIT_SUCCESS;
}
