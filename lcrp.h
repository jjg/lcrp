/*
  Calculates the best Lipschitz constant for the radial
  projection on the space of sequences with the p-norm using
  the method described in [2] based on the main  result of [1].

  The value of p should be between 1 and infinity, the result
  is assigned to the second argument.

  The function returns zero on success and nozero on failure,
  in the latter case errno may be set.  The return value can
  be used as imput for the function  lcrp_strerror() which
  returns a suitable error message.

  [1] Carlo Franchetti, "The Norm of the Minimum Projection
  onto Hyperplanes in L^p[0,1] and the Radial Constant", Boll.
  Un. Mat. Ital. 4-B (7), pp. 803-821, 1990.

  [2] J.J. Green, "The Lipschitz constant for the radial
  projection on real lp -- implementation notes", unpublished
  technical note, 2012.

  Copyright (c) 2011, 2012, 2015,  J.J. Green

MIT License
*/

#ifndef LCRP_H
#define LCRP_H

#ifdef __cplusplus
extern "C"
{
#endif

#define LCRP_OK 0
#define LCRP_EDOM 1
#define LCRP_NR_ZD 2
#define LCRP_NR_ESC 3
#define LCRP_NR_NOCONV 4

int lcrp(double, double*);
const char* lcrp_strerror(int);

#ifdef __cplusplus
}
#endif

#endif
