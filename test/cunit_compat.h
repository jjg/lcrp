/*
  cunit_compat.h

  Compatibility macros to handle different versions of CUnit,
  in particular the changes to the suite struct introduced in
  version 2.1-3

  Copyright (c) J.J. Green 2016
*/

#ifndef CUNIT_COMPAT_H
#define CUNIT_COMPAT_H

/* default to 2.1-3 or later (for now) */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef CUNIT_213
#define CUNIT_213 1
#endif

#if CUNIT_213
#define CU_Suite_Entry(a, b, c, d) {(a), (b), (c), NULL, NULL, (d)}
#else
#define CU_Suite_Entry(a, b, c, d) {(a), (b), (c), (d)}
#endif

#endif
