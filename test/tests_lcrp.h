/*
  tests_lcrp.h

  Copyright (c) J.J. Green 2015
*/

#ifndef TESTS_LCRP_H
#define TESTS_LCRP_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_lcrp[];

extern void test_lcrp_known(void);
extern void test_lcrp_conjugate(void);
extern void test_lcrp_edom(void);
extern void test_lcrp_asymp_one(void);
extern void test_lcrp_asymp_two(void);

#endif
