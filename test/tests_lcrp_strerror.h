/*
  tests_lcrp_strerror.h
  Copyright (c) J.J. Green 2015
*/

#ifndef TESTS_LCRP_STRERROR_H
#define TESTS_LCRP_STRERROR_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_lcrp_strerror[];

extern void test_lcrp_strerror_ok(void);
extern void test_lcrp_strerror_msg_ok(void);
extern void test_lcrp_strerror_msg_unknown(void);

#endif
